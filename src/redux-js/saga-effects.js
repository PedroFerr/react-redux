import { takeEvery, call, put } from "redux-saga/effects";

// Possible App's Redux Actions that will use Saga:
import { DATA_REQUEST, DATA_LOADED, DATA_API_ERRORED } from "./actions";

// What Saga should listen to:
export default function* watcherSaga() {
    yield takeEvery(DATA_REQUEST, workerSaga);
}

// What Saga should "effect" on the selected listned Actions, triggering result returned Actions
function* workerSaga(inputAction) {
    try {
        const
            request = inputAction.payload
            , response = yield call(getData, request)
            
        ;
        yield put({ type: DATA_LOADED, payload: response });
    } catch (e) {
        console.log('Error @ workerSaga', e);
        yield put({ type: DATA_API_ERRORED, payload: {error: `API fetch ${e}`} });
    }
}

function getData(payload) {
    
    return fetch(payload).then(response => {
            console.log('Fetching on «' +payload+ '» API!');
            if (!response.ok) {
                throw Error(`status: ${response.status} | type: ${response.type} | description: ${response.statusText}`);
            }
            return response.json()
        }
    );

    // return fetch(payload)
    //     .then(response => {
    //         if (!response.ok) {
    //             throw Error(`status: ${response.status} | type: ${response.type} | description: ${response.statusText}`);
    //         }
    //         return response.json();
    //     })
    //     .then(json => {
    //         dispatch({ type: DATA_LOADED, payload: json });
    //     })
    //     .catch(error => {
    //         console.error('There was an API fetch ERROR:', error);
    //         dispatch({ type: DATA_LOADED, payload: {error: `API fetch ${error}`} });
    //     });
}