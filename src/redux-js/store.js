import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers";

import thunk from "redux-thunk";
// ... or we use Saga:
import createSagaMiddleware from 'redux-saga'; 
import apiSaga from '../redux-js/saga-effects';

const
    storeEnhancers  = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    , initialiseSagaMiddleware = createSagaMiddleware()    
    , store = createStore(
        rootReducer
        , storeEnhancers(
            applyMiddleware(
                thunk
                // , other store enhancers if any
                // https://github.com/zalmoxisus/redux-devtools-extension#12-advanced-store-setup
                // https://www.valentinog.com/blog/redux/
                , initialiseSagaMiddleware
            )
        )
    )
    
;

// Run our Saga:
initialiseSagaMiddleware.run(apiSaga);

export default store;