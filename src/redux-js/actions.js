export const ADD_ARTICLE = "ADD_ARTICLE";

export const DATA_REQUEST = "DATA_REQUEST";
export const DATA_LOADED = "DATA_LOADED";           //=> allthough doesn't need a dispatcher anymore (using Saga), will be imported @ reducers.js
export const DATA_API_ERRORED = "DATA_API_ERRORED"  //=> allthough doesn't need a dispatcher, will be imported @ reducers.js

export function addArticle(payload) {
    return { type: ADD_ARTICLE, payload }
};

export function getData(payload) {
    // USING Redux Thank: The result payload, to dispatch to the reducer, of this Action will come from an API fetch:
    /*
    return function (dispatch) {
        return fetch(payload)
            .then(response => {
                if (!response.ok) {
                    throw Error(`status: ${response.status} | type: ${response.type} | description: ${response.statusText}`);
                }
                return response.json();
            })
            .then(json => {
                dispatch({ type: DATA_LOADED, payload: json });
            })
            .catch(error => {
                console.error('There was an API fetch ERROR:', error);
                dispatch({ type: DATA_LOADED, payload: {error: `API fetch ${error}`} });
            });
    };
    */

    // USING powerfull Redux Saga: Actions are simpler - they JUST trigger Saga - having a clear separation between synchronous and asynchronous logic
    // Saga becomes a completely separated thread from your App and from the Redux - it JUST dispatch Actions that will produce side effects @ Saga
    // Saga will, then, depending on the obtained responses, trigger new Actions to inject again at the Redux reducers, filling data containers.
    return { type: DATA_REQUEST, payload }; 
}

