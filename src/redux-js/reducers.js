import { ADD_ARTICLE, DATA_LOADED, DATA_API_ERRORED } from "./actions";

const initialState = {
    articles: []
    , remoteArticles: []
};

/**
 * AVOID mutations, on REDUX - NEVER copy, add, remove or change data; create a NEW state, preserving the former one!
 *      on ARRAYS, using concat(), slice(), and …spread
 *      on OBJECTS, using Object.assign() and …spread
 * 
 * @param {*} state 
 * @param {*} action 
 */
function rootReducer(state = initialState, action) {

    if (action.type === ADD_ARTICLE) {
        return Object.assign({}, state, {
            articles: state.articles.concat(action.payload)
        });
    }

    if (action.type === DATA_LOADED || action.type === DATA_API_ERRORED) {
        return Object.assign({}, state, {
            remoteArticles: state.remoteArticles.concat(action.payload)
        });
    }

    return state;
};

export default rootReducer;