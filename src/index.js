import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './redux-app/App.jsx'; // './App';
import * as serviceWorker from './serviceWorker';

import { Provider } from "react-redux";
import store from './redux-js/store';


/**
 * Provider (high order component coming from "react-redux") wraps up the entire React application
 * Will get the Redux Store as a prop, to stay in control of the whole App scope
 */
ReactDOM.render(
    // <App />
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
