import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import store from './redux-js/store';
import { addArticle } from './redux-js/actions';

/**
 * Store vars in window so we can console.log PLAY with them!
 * 
 * Next should display "{articles: Array(0)}"
 * store.getState()
 * 
 * To change the state in Redux we need to dispatch an action:
 * store.dispatch( addArticle({ title: 'React Redux Tutorial for Beginners', id: 1 }) ) 
 *
 * Subscribe to any changes at the Store:
 * store.subscribe(() => console.log('Look ma, Redux!!'))
 * (you should see the text, after dispatching ANY Action - i.e. the previous one!)
 * 
 * Now "store.getState()" should display 1 object: { title: 'React Redux Tutorial for Beginners', id: 1 }
 * If you keep dispatching more Actions, our Store will increase the number of objects
 * (as block commented @ './redux-js/reducers/index.js', we always create a NEW state, preserving the former one!)
 * 
 */
window.store = store;
window.addArticle = addArticle;

class App extends Component {
    
    render() {
        return (
            <div className="App">
                <header>
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>Edit <code>src/App.js</code> and save to reload.</p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >Learn React</a>
                </header>
                <main>
                    Our Redux Store is ON! Just console.log play with it!:
                    <br />
                    (see App.js file code - top block documentation - for that)
                </main>
            </div>
        );
    }
}

export default App;
