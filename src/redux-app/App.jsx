import React from "react";

import List from "./components/List";
import Form from './components/Form'
import Post from './components/Post';

const App = () => (
    <main>
        <div className="row mt-5">
            <div className="col-md-4 offset-md-1 mb-5">
                <h2>Articles</h2>
                <List />
            </div>

            <div className="col-md-4 offset-md-1 mb-5">
                <h2>Add Article</h2>
                <Form />
            </div>
        </div>

        <div className="row mt-5">
            <div className="col-md-10 offset-md-1">
                <h2>10 top Posts API result</h2>
                <Post />
            </div>
        </div>

    </main>
);

export default App;