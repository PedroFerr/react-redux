import React, { Component } from 'react';
import { connect } from 'react-redux';
import uuidv1 from 'uuid';

import { addArticle } from '../../redux-js/actions';

function mapDispatchToProps(dispatch) {
    return {
        addArticle: article => dispatch(addArticle(article))
    };
}

class ConnectedForm extends Component {
    constructor() {
        super();

        this.state = {
            inputValue: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ inputValue: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const
            title = this.state.inputValue
            , id = uuidv1()
        ;
        this.props.addArticle({ title, id });
        this.setState({ inputValue: '' });
    }

    render() {
        const
            { inputValue } = this.state
            
        ;

        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        value={inputValue}
                        onChange={this.handleChange}
                    />
                </div>
                <button type="submit" className="btn btn-success mx-auto" style={{display: 'block'}}>SAVE to the STORE</button>
            </form>
        );
    }
}

const Form = connect(null, mapDispatchToProps)(ConnectedForm);

export default Form;