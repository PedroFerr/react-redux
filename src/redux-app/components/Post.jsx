import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getData } from '../../redux-js/actions';

const mapStateToProps = state => {
    return { posts: state.remoteArticles.slice(0, 10) };
}

export class Post extends Component {

    componentDidMount() {
        const
            endPoint = 'https://jsonplaceholder.typicode.com/posts'
            
            // Simulate API error:
            // endPoint = 'https://jsonplaceholder.typicode.com/po'
            // Simulate no returning data:
            // endPoint = 'https://jsonplaceholder.typicode.com/posts?userId=-1'

        ;
        this.props.getData(endPoint);
    }

    render() {
        return (
            <div className="api-fetch-container">
            {   
                this.props.posts.length === 0
                    &&
                <div className="alert alert-warning">
                    Data is empty...
                </div>
            }

            {   
                this.props.posts.length > 0 && this.props.posts[0] && !this.props.posts[0].error
                    &&
                <ul className="list-group list-group-flush">
                    {   
                        this.props.posts.map(el => (
                            <li className="list-group-item" key={el.id}>
                                {el.title}
                            </li>
                        ))
                    }
                </ul>
            }

            {   
                this.props.posts.length > 0 && this.props.posts[0].error
                    &&
                <div>
                    {
                        this.props.posts.map(el => (
                            <div className="alert alert-danger" key="0">
                                {el.error}
                            </div>
                        ))
                    }
                </div>
            }
            </div>
        );
    }
}

const
    PostsList = connect(
        mapStateToProps,
        { getData }
    )(Post)
    
;

export default PostsList;